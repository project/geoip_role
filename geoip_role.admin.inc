<?php

/**
 * @file
 * Written by Henri MEDOT <henri.medot[AT]absyx[DOT]fr>
 * http://www.absyx.fr
 */

function geoip_role_admin_general($form_state) {
  $roles = user_roles(TRUE);
  unset($roles[DRUPAL_AUTHENTICATED_RID]);

  $form['roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Controlled roles'),
    '#options' => $roles,
    '#default_value' => array_keys(geoip_role_roles()),
    '#description' => t('Check the roles to be controlled by GeoIP Role. You should only check roles that have been created exclusively for GeoIP Role.'),
  );
  $form['iso3166'] = array(
    '#type' => 'textfield',
    '#title' => t('Country list file path'),
    '#default_value' => variable_get('geoip_role_iso3166', 'http://www.iso.org/iso/iso3166_en_code_lists.txt'),
    '#description' => t('Enter the path to the file containing the list of the country names and ISO 3166-1-alpha-2 codes. It is recommended to download this file at <a href="!url">!url</a> and to copy it into the %dir directory. You should then enter %path as the path.', array(
      '!url' => 'http://www.iso.org/iso/iso3166_en_code_lists.txt',
      '%dir' => 'sites/all',
      '%path' => 'sites/all/iso3166_en_code_lists.txt',
    )),
    '#after_build' => array('geoip_role_iso3166_check'),
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  return $form;
}

function geoip_role_iso3166_check($form_element, &$form_state) {
  $path = $form_element['#value'];

  if ((substr($path, 0, 7) != 'http://') && !file_exists($path)) {
    form_error($form_element, t('The path %path does not exist.', array('%path' => $path)));
  }

  return $form_element;
}

function geoip_role_admin_general_submit($form, &$form_state) {
  variable_set('geoip_role_roles', array_filter($form_state['values']['roles']));
  variable_set('geoip_role_iso3166', $form_state['values']['iso3166']);
  drupal_set_message(t('The configuration options have been saved.'));
}



function geoip_role_admin_country_roles($form_state) {
  $roles = geoip_role_roles();
  $options = array();
  foreach ($roles as $rid => $name) {
    $options[$rid] = '';
    $form['role_names'][$rid] = array('#value' => $name);
  }

  $countries = geoip_role_countries();
  $country_roles = geoip_role_country_roles();
  foreach ($countries as $code => $name) {
    $form['country_names'][$code] = array('#value' => $name);
    $form['country_roles'][$code] = array(
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => isset($country_roles[$code]) ? array_keys($country_roles[$code]) : array(),
    );
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  return $form;
}

/**
 * Theme the administer country roles page.
 *
 * @ingroup themeable
 */
function theme_geoip_role_admin_country_roles($form) {
  $header[] = t('Countries');
  foreach (element_children($form['role_names']) as $rid) {
    if (is_array($form['role_names'][$rid])) {
      $header[] = array('data' => drupal_render($form['role_names'][$rid]), 'class' => 'checkbox');
    }
  }

  foreach (element_children($form['country_roles']) as $code) {
    if (is_array($form['country_roles'][$code])) {
      $row = array();
      $row[] = drupal_render($form['country_names'][$code]);
      foreach (element_children($form['country_roles'][$code]) as $rid) {
        if (is_array($form['country_roles'][$code][$rid])) {
          $row[] = array('data' => drupal_render($form['country_roles'][$code][$rid]), 'class' => 'checkbox',
            'title' => $form['role_names'][$rid]['#value'] .' : '. $form['country_names'][$code]['#value']);
        }
      }
      $rows[] = $row;
    }
  }

  $output  = theme('table', $header, $rows);
  $output .= drupal_render($form);
  return $output;
}

function geoip_role_admin_country_roles_submit($form, &$form_state) {
  $values = array();
  foreach ($form_state['values'] as $code => $roles) {
    if (is_array($roles)) {
      $roles = array_filter($roles);
      if (!empty($roles)) {
        $values[$code] = $roles;
      }
    }
  }
  variable_set('geoip_role_country_roles', $values);
  drupal_set_message(t('The configuration options have been saved.'));
}
